# Scenarios

Hey storytellers, welcome to this git repository dedicated to free, open and collaborative creation of scenarios for the webcomic project [Pepper&Carrot](http://wwww.peppercarrot.com). This repository exist in the purpose to build together a better entertaining experience around Pepper&Carrot.

## Pepper&Carrot style:



We use **markdown** format and english. Check the other scenarios in this repository to have an idea of the formating style. The file **episode-template.md** can help you to get started.

1. Short stories; between 20 and 30 panels.
2. One action per panel and not too much characters.
3. Less speech-bubble is always better.
4. Funny, Suprising or non-expected ending.
5. Monsters, epic landscape, special Fx: think big and cinematic!
6. Many information about the world are on the [Wiki](https://github.com/Deevad/peppercarrot/wiki).
6. No sex, no violence; all audience.

## License:

Authors of all modifications, corrections or contributions to the Wiki accept to release their work under the license: [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
