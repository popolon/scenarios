Episode 25: There are no shortcuts
==================================

_(Alternative title: The Butterfly Effect, Chaos theory.)_

**Pitch:** Muted episode (constraint: no speech bubbles). After ep24, the house of Pepper is a ruin. Thym, Cayenne and Cumin will show a powerful demonstration of Chaosah to Pepper and rebuild the house combining their power. Pepper will totally be excited by the spectacle and be eager to know how to do this kind of magic. We will see there are no shortcuts; and Pepper's excitement will not last for too long when she sees that she now just has more books to study...

**Notes:** This episode restores the power of the old witches Thym, Cayenne, Cumin; we rarely saw them using Chaosah's magic in group. It's also restoring the house of Pepper after destroying it in the previous episode. Also, the ending will have Pepper studying, so we'll be ready to jump directly to "The exam" in episode 26.

**page 01**

- External view and global shot on the house of Pepper. The house is now a ruin, the roof is smoking; a part of the Unity Tree branches are burned, probably by micro-suns. The morning light and first rays of sun on it add drama to the scene. Last night was probably tough after the partial explosion of the house (see previous episode, ep24 "The Unity Tree"). In front of this landscape of desolation stand our four witches –Cayenne, Cumin, Thym, Pepper– and one cat, Carrot.
- View on them all, side by side. They all have scratched clothes and dust on them. Some smoke in the foreground. The old godmothers look at the scene seriously. Pepper is still embarassed but looks at them with curiousity. What are they gonna do?
- Thym tells something to Cayenne, making a spiral with her finger. Cayenne looks at it very seriously and understands it as an order.
- Cayenne closes her eyes with hands folded together. A weird energy aura develops around her.

```
+--------+
|        |
|        |
+--------+
|        |
+-----+--+
|     |  |
+-----+--+
```

**page 02**

- Suddenly Cayenne opens her eyes and makes a large gesture with both hands, releasing a big wave of energy.
- The ruin of the house explodes suddenly in a large cloud of thin particles.
- Side view on Pepper extending her arm to touch the flying, colorful particles that are floating in the air with her hand. She is fascinated.
- Close up on her finger with a larger view on the particles; they are made of little cubes, pyramids, cylinders, spheres. Primitive small 3D shapes of the same color as the roof tiles, stones, etc...
- Cayenne in the background readjusts her hat as an old cowboy would do. Nothing spectacular for her, it looks like she just did a routine spell. Pepper in the foreground turns her head to look at her and is again fascinated. *Cayenne is really that powerful?*


```
+--+-----+
+--+     |
|        |
+-----+--+
|     |  |
+-----+--+
|        |
+--------+

```

**page 03**

- Thym now speaks to Cumin and describes a specific gesture with two fingers. Cumin nods her head to show that she understood the order.
- Cumin forms a circle of light with both hands and weird runes, red/violet symbols, are appearing as a cylinder column.
- Close up, a red shiny little butterfly made of light appears above the palm of Cumin. It's a wonderful and beautiful small creature with an infinity symbol as a pattern on its wings and with a twisted horn like a demon. It's impressive and thinly sculpted as a precious jewel but it also feels dangerous. Under it, a circle of runic incantations shows the audience it's a summoned creature; probably an important one.
- Pepper's jaw drops, she's looking amazed at Cumin. Carrot has the exact same expression next to her. *Cayenne, okay; but Cumin can do that?!* Cumin looks serious, and is lit heroically by the strong, reddish energy of the butterfly. Her hair is flying a bit, her cape too.

```
+-----+--+
|     |  |
+-----+--+
|        |
+--------+
|        |
+--------+
```

**page 04**

- Bird view to get a general view on the scene now: the godmothers plus Pepper are in front of the giant cloud of particles where the house was standing previously. Particles near the camera are more visible, the ones near the floor look like a cloud of dust. Thym unfolds her tiny arms toward Cumin. Cumin gives her the tiny red butterfly from her two palms has if it was a very fragile present. Pepper is fascinated by the scene, Carrot is hiding behind one of Pepper's legs, a bit affraid.
- Side view on the old Thym, smiling with the butterfly now flying over her hand (in a position similar to how a hawk/falcon trainer would let his bird land on his hand). Her smile plus the red light make the scene a bit mysterious.
- Close up: front view on Thym. (red butterfly in foreground, out of focus so really blury).
- Close up: front view on Thym closing her eyes.
- Close up (zoomed in further): front view on Thym closing her eyes with a dark background. We are entering in her head.

```
+--------+
|        |
|        |
+--------+
|        |
+--+--+--+
|  |  |  |
+--+--+--+
```

**page 05**

- Large shot with Thym in little alone over a pure black background with a thin dimensional perspecitive grid on the floor. A lot of particles are flying around her and feels now very colorful and digital object. The ones closer to the camera are tiny 3D meshes with a wireframe visible. Some [mathematical expressions](https://en.wikipedia.org/wiki/Chaos_theory) are also flying around. Thym is sweats a bit and is intensely focused; she is obviously waiting for the right moment.
- Symmetrical shot on the face of Thym who suddenly opens her eyes; while the background around her head is still made of particles over a grid. But now, every particle has vector trajectories visible, arrows, and everything looks linked to everything. Thym gets a flash of "the big picture" at this moment.
- Large side view with Thym now having a position like someone with a bazooka at the place of the hand (the one with the butterfly flying over it). She uses her second hand to maintain her arm because the butterfly is now flapping its wings at full speed; one can only see the motion blur. She screams an order at the butterfly to trigger the action effect. The light around the butterfly also intensifies a lot, showing the power of this incantation. The particles on the right of the panel are immediately affected by the wings of the butterfly and start describing turbulence patterns and the shorter turbulence pattern here and there combines into larger vortexes.

```
+--------+
|        |
+--------+
|        |
+--------+
|        |
+--------+
```

**page 06**

- (combination of multiple panels with chaotic frames): Close up in the particles swirling: we can guess a part of the tile pattern of the roof is reconstructing on the tornado of particles. Another close up in the particles swirling: a part of the front door with the lantern is being reconstructed here! In the background, bigger parts are also being reconstructed. Another close up in the particles swirling: the telescope of Pepper coming back to one piece!
- Extreme close up on Pepper's face: she almost has tears of joy, we can guess it's the best day of her life; she now sees the power of Chaosah, the power of her godmothers! The spectacle is everything she'd hoped for in the previous 24 episodes.
- Wide bird view: the shape of the full house is now here; small blocks are still combining themselves to produce details. Particles are still flying around but they are now only few lines made of particles around the part not finished. Pepper's house looks as good as new. Something really contrasting the ruin of panel 1.



```
+--+--+--+
|   \/   |
|   /\   |
+--+--+--+
|  |     |
|  |     | 
|  |     |
+--+-----+
```

**page 07**

- Shot on the three old witches of Chaosah. They're not even smirking and they look at the job done as if it was "just another trick they saw hundreds of time". The three of them look heroic and badass at this moment.
- Pepper is excited and turns around to the three godmothers like a fangirl with many questions for her three heroines. She is drawn multiple times, to suggest she bombards each of the witches with questions. Holding their hands, expressing "Wow", looking at the sky with her hand suggesting a "OMG!!". We understand Pepper is super excited. For this panel, Pepper can have speechbubbles with small pictures instead of text. The drawing to represent the question for Cayenne is "mini particles, question mark", for Cumin it's "butterfly, question mark" and for Thym "vortex, question mark". Thym can smile slightly in this panel, satisfied Pepper is now motivated to learn something.
- Long cut, Pepper is now inside the library (as in the previous episode). It's a general shot. Her excitement is now reduced to zero: two big stacks of books are now on her desk. It's not the type of answer Pepper was expecting and she is of course discouraged to see there are no shortcuts to learn this type of magic and she has to study all the books anyway. The three witches are leaving the book library with what we can guess is a smile.

_~FIN~_

```
+--------+
|        |
+--------+
|        |
+--------+
|        |
+--------+
```

