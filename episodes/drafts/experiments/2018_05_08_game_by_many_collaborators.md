Experiment
==========

**Rules**: each participant writes one panel and tries to be consistent with previous panels. Then he designs the next participant.

---

@valvin

We are in Pepper's bedroom during the middle of the night. Pepper is deeply sleeping while Carrot is just finding something in a chest. Carrot is trying to not make noise. He has in hand a costume looking like a super hero costume which can hide him enough to be not recognizable.

---

@xHire

Carrot is half on his way out of the chest (one leg down), carefully holding the costume in his mouth, paws on edge of the chest. His head and eyes are turned towards Pepper’s bed—making sure she still sleeps. He’s cautious not to have anyone notice him.

---

@imsesaok

The metal button on the costume fell out and made a clanging noise. Carrot freaked out and jumped into the air. The chest door hit the wall and made a loud bang.

---

@Midgard

Pepper turns around and groans. Carrot looks at her anxiously. Will she wake up?

---

@popolon


