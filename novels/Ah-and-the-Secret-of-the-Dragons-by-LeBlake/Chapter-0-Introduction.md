# Ah and the Secret of the Dragons

## Chapter 0: Introduction
--------------------------------------------
"_In the beginning, there was the Nothingness, a disorderly connundrum of paradoxes opposed to their very own existence. Then, matter was brought forth by the forces of Chaos' mere opposition to it. Reality took hold of Chaos and grew out of it, creating uncountable planes of existence and giving birth to the material universe, with its galaxies, stellar systems and planets. And from within the planets there emerged life as an expression of Chaos itself. Innumerable living beings were born and died forever disappearing, but there was one creature that perdured through the eons. The earth would tremble with their feet, the skyes would shimmer with the reflections of their scales and the far reaches of many worlds would echo with their voices, for they were the Dragons, the Zenith of Creation and the true children of Chaos and Evolution, and the reigned the material plane unchallenged by anyone. But very recently, a newcomer to existence, a small creature of ingenious nature, asked itself about the role of dragons in their everchanging world, Hereva. And that is when the oldest dragons took notice of them and their Great Tree and began speaking to them in riddles and playing with them as if they were ants with too many questions._

The rest, you know it already."

**All-We-Know, witch of the Swamp of the Dying Path**
